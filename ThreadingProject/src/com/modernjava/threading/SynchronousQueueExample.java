package com.modernjava.threading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

public class SynchronousQueueExample {

	public static void main(String[] args) {
		final String[] names = {"mike", "syed", "jean", "rajeev"}; 
		final SynchronousQueue<String> queue = new SynchronousQueue<String>();
		
		Runnable producer = () ->{
			for (String name: names) {
				try {
					queue.put(name);
					System.out.println("Inserted: " + name + " in the queue"); 
					TimeUnit.MILLISECONDS.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
		}; 
		
		Runnable consumer = () -> {
			while(true) {
				try {
					System.out.println ("Retrieved: " + queue.take() + " from the queue"); 
					TimeUnit.MILLISECONDS.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		
		ExecutorService executor = Executors.newFixedThreadPool(2); 
		executor.submit(producer); 
		executor.submit(consumer); 
		executor.shutdown();
	}

}
