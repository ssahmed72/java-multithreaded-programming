package com.modernjava.threading.stream;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamExample {

	public static void main(String[] args) {
		//Map Operation 
		List instructorList = InstructorDataBase.getAllInstructors()
								.stream()
								.map(s->s.getName())
								.collect(Collectors.toList()); 
		System.out.println (instructorList); 
		
		//ToMap Operation
		Map<String, List<String>> map = InstructorDataBase.getAllInstructors()
										.stream()
										.collect(Collectors.toMap(Instructor::getName, Instructor::getCourses)); 
		
		System.out.println (map);
		
		//Filter
		map = InstructorDataBase.getAllInstructors()
								.stream()
								.filter(s->s.getName().startsWith("M"))
								.collect(Collectors.toMap(Instructor::getName, Instructor::getCourses));
		System.out.println(map);
		
		//Sorted
		instructorList = InstructorDataBase.getAllInstructors()
											.stream()
											.sorted(Comparator.comparing(Instructor::getName))
											.collect(Collectors.toList()); 
		
		System.out.println(instructorList);
		
		//ForEach
		InstructorDataBase.getAllInstructors().stream().forEach(System.out::println);
		
		//Reduce
		List<Integer> integers= InstructorDataBase.getAllInstructors()
												.stream()
												.map(s->s.getNumOfCourse())
												.collect(Collectors.toList()); 
		
		Integer result = integers.stream().reduce(0, (a,b) -> a+b); 
		System.out.println ("Total number of courses: " + result); 
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	

}
