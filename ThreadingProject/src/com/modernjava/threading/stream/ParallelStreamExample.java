package com.modernjava.threading.stream;

import java.util.stream.IntStream;

public class ParallelStreamExample {

	public static void main(String[] args) {
		System.out.println ("CPU's in this machine: " + Runtime.getRuntime().availableProcessors());
		singleStreamExample(); 
		parallelStreamExample(); 

	}
	
	public static void singleStreamExample() {
		long startTime = System.currentTimeMillis();
		int i=100000; 
		int sum= IntStream.rangeClosed(0, i).sum();
		System.out.println ("Calculating sum of " + i + " in a single stream: " + sum); 
		System.out.println("It took: " + (System.currentTimeMillis() - startTime) + " in msec to calculate sum of " 
		+ i + " numbers in single Thread"); 
	}
	
	public static void parallelStreamExample() {
		long startTime = System.currentTimeMillis();
		int i=100000; 
		int sum= IntStream.rangeClosed(0, i).parallel().sum();
		System.out.println ("Calculating sum of " + i + " in a parallel stream: " + sum); 
		System.out.println("It took: " + (System.currentTimeMillis() - startTime) + " in msec to calculate sum of " 
		+ i + " numbers in parallel Thread"); 
	}

}
