package com.modernjava.threading;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class TraditionalVsConcurrentExample {

	public static void main(String[] args) {
		//concurrentModificationWithArrayList();
		concurrentModificationWithConcurrentList();
	}
	
	public static void concurrentModificationWithArrayList() {
		List<Integer> list = new ArrayList<Integer>(); 
		for (int i=0;i<100000; i++) {
			list.add(i);
		}
		
		new Thread(() -> {
			try {
				System.out.println ("Adding 5000 to the list"); 
				list.add(5000); 
			}catch (ConcurrentModificationException e) {
				e.printStackTrace();
			}
		}).start();
		
		
		Iterator<Integer> it = list.iterator(); 
		while(it.hasNext()) {
			it.next(); 
		}
	}
	
	public static void concurrentModificationWithConcurrentList() {
		System.out.println ("Running with Concurrent List"); 
		List<Integer> list = new CopyOnWriteArrayList<Integer>(); 
		
		for (int i=0;i<100000; i++) {
			list.add(i); 
		}
		
		new Thread(() -> {
			try {
				System.out.println ("Adding 5000 to the list"); 
				list.add(5000); 
			}catch (ConcurrentModificationException e) {
				e.printStackTrace();
			}
		}).start();
		
		Iterator<Integer> it = list.iterator(); 
		while(it.hasNext()) {
			it.next(); 
		}
		
	}

}













