package com.modernjava.threading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class LinkedBlockingQueueExample {

	public static void main(String[] args) {
		LinkedBlockingQueue<Integer> linkedBlockingQueue = new LinkedBlockingQueue<>(5); 
		
		Runnable producer = () -> {
			int i=0; 
			while (true) {
				try {
					linkedBlockingQueue.put(++i);
					System.out.println ("Added: " + i); 
					TimeUnit.MILLISECONDS.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		};
		
		Runnable consumer = () -> {
			while (true) {
				Integer poll;
				try {
					poll = linkedBlockingQueue.take();
					System.out.println ("Polled: " + poll); 
					TimeUnit.MILLISECONDS.sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} 
				
			}
		};
		
		ExecutorService executorService = Executors.newFixedThreadPool(2); 
		executorService.submit(producer); 
		executorService.submit(consumer); 
		executorService.shutdown();
	
	}

}
