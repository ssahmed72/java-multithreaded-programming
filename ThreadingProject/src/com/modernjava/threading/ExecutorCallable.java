package com.modernjava.threading;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ExecutorCallable {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		Callable<String> callableTask = () -> {
			TimeUnit.MILLISECONDS.sleep(1000);
			return "Current time is: " + LocalDateTime.now();
		};
		
		ExecutorService executor = Executors.newFixedThreadPool(1); 
		List<Callable<String>> taskList = Arrays.asList(callableTask, callableTask); 
		
		System.out.println ("First example using invokeAll");
		List<Future<String>> results = executor.invokeAll(taskList); 
		
		for (Future<String> result: results) {
			System.out.println(result.get()); 
		}
			
		System.out.println("Executing callable taskList using submit"); 
		Future<String> result = executor.submit(callableTask); 
		
		while (!result.isDone()) {
			System.out.println ("The method return value: " + result.get()); 
		}
		
		executor.shutdown();
		
	}

}
